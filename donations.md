% lifprint — Donations
% Bastien Nocera, Daniel Drake
% 2018

# Donations

## Previous hardware donors

* Tony Vroon
* Gerrie Mansur from [Security Database BV](http://www.securitydatabase.net/)
* Joaquin Custodio
* [TimeTrex](http://www.timetrex.com/)
* Gavin Smalley
* [System76](http://www.system76.com/)
* [Covadis](http://www.covadis.ch/)
* [Apricorn](http://www.apricorn.com)
* [Goodix](https://www.goodix.com/)
* [Synaptics](https://www.synaptics.com/)
* [Elan](http://www.emc.com.tw/)

## Previous Monetary donations

* Stanislav Lechev
* James Klaas
* Wolfram Schlich
* Markus Haindl
* Rossano Savino
* Matthias Schmidt
* Tony Vroon
