% Fingerprint reader support
% Bastien Nocera, Daniel Drake
% 2018

![Front](images/Fprint_logo.png)

# Fingerprint reader support

The fprint project aims to add support for consumer fingerprint reader devices, in Linux, as well as other free Unices.

Previously, Linux support for such devices has been scattered amongst different projects (many incomplete) and inconsistent in that application developers would have to implement support for each type of fingerprint reader separately. For more information on where we came from, see [the project history page](project-history.html).

We're trying to change that by providing a central system to support all the fingerprint readers we can get our hands on. The software is free software and in the long term we're shooting for adoption by distributions, integration into common desktop environments, etc.

Both libfprint and fprintd's source code is available through git, as well as released archives, reachable through their respective GitLab pages.

## Integration

The GNOME desktop [supports fingerprint management](https://help.gnome.org/users/gnome-help/stable/session-fingerprint.html.en) through its **Users** Settings panel. Most Linux distributions support fingerprint login through fprintd.

The OpenBSD operating system also has login integration through [login_fingerprint](http://blade2k.humppa.hu/login_fingerprint-1.2.tar.gz).

## libfprint

libfprint is the centre of our efforts. libfprint is the component which does the dirty work of talking to fingerprint reading devices, and processing fingerprint data.

If you're a user, you probably aren't interested in libfprint, instead you want to find some software which uses libfprint (see Integration section above).

libfprint is of prime interest to developers wishing to add support for specific fingerprinting hardware.

- [Supported devices](supported-devices.html)
- [GitLab page](https://gitlab.freedesktop.org/libfprint/libfprint)
- [Releases](https://gitlab.freedesktop.org/libfprint/libfprint/-/releases)
- [stable (2.0) API documentation](libfprint-dev/)

## fprintd

fprintd is a daemon that provides fingerprint scanning functionality over D-Bus. This is the software developers will want to integrate with to add fingerprint authentication to OSes, desktop environments and applications. It also includes a PAM module to implement user login (`pam_fprintd` replacing the obsolete `pam_fprint` module), and small command-line utilities if your desktop environment does not integrate support.

- [GitLab page](https://gitlab.freedesktop.org/libfprint/fprintd)
- [Releases](https://gitlab.freedesktop.org/libfprint/fprintd/-/releases)
- [API documentation](fprintd-dev/)

## Miscellaneous links

- [Mailing list](https://lists.freedesktop.org/mailman/listinfo/fprint)
- [Contributing](https://gitlab.freedesktop.org/libfprint/libfprint/blob/master/HACKING.md)
- [US Export Control](us-export-control.html)
- [Donations](donations.html)

## Obsolete and incompatible software

While looking for information about fingerprint reader support in Linux and other free OSes, you might find references to the following software. Those are either obsolete, or not related to the fprint project.

- `pam_fprint` is replaced by fprintd's `pam_fprintd` module, which splits the PAM conversation from hardware access.
- `fprint_demo` is obsolete. Most features are integrated into fprintd's helpers, and libfprint has an examples section.
- libfprint 1.0 ([old API](libfprint-stable/)) is deprecated and not supported anymore
