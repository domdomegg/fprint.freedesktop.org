# Sources for the fprint website

Those are the sources of the libfprint/fprintd website:
https://fprint.freedesktop.org/

Pages are generated through GitLab's CI (see `.gitlab-ci.yml`),
using [Discount](http://www.pell.portland.or.us/~orc/Code/discount/).

## Old website

Sources are available in the freedesktop.org Wiki's git repository.
[front page](https://cgit.freedesktop.org/wiki/www/tree/Software/fprint.mdwn?id=56ddf0f43ba7d98b774fbb82f6f7ae6b1781009a)
[hierarchy](https://cgit.freedesktop.org/wiki/www/tree/Software/fprint?id=26c53ff20a0e44ec3a48e8d7e1c1f6c0a65400d3)
