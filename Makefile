.NOTPARALLEL:

HTML=index.html project-history.html security-notes.html us-export-control.html supported-devices.html donations.html

all: ${HTML}

supported-devices.md:
	libfprint/_build/libfprint/fprint-list-supported-devices > $@

%.html : %.md
	discount-mkd2html -css simple.css $<

install: ${HTML}
	cp -r images/ ${HTML} simple.css public/

clean:
	rm -f ${HTML}
